<?php

/**
 * @file Unfuddle API Drupal module hooks and API.
 */

/**
 * Implement hook_permission().
 */
function unfuddle_api_permission() {
  return array(
    'administer unfuddle api configuration' => array(
      'title' => t('Administer Unfuddle API configuration'),
      'description' => t('Configure integration with your Unfuddle.com account.'),
    ),
  );
}

/**
 * Implement hook_menu().
 */
function unfuddle_api_menu() {
  $items = array();
  
  $items['admin/config/services/unfuddle_api'] = array(
    'title' => 'Unfuddle API',
    'description' => 'Adjust settings for Unfuddle',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('unfuddle_api_admin_settings'),
    'access arguments' => array('administer unfuddle api configuration'),
  );
  
  return $items;
}

/**
 * Administration form for Unfuddle API connection strings stored in the variable table.
 */
function unfuddle_api_admin_settings() {
  $form = array();

  $form['#prefix'] = t('Use the following form to enter your Unfuddle connection settings.');

  $form['unfuddle_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of the Unfuddle instance'),
    '#description' => t('The Unfuddle URL, ensure you enter the FULL URL with leading protocol (http:// OR https://) and NO trailing slash'),
    '#default_value' => variable_get('unfuddle_api_url', ''),
    '#required' => TRUE,
  );
  $form['unfuddle_api_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Unfuddle user'),
    '#default_value' => variable_get('unfuddle_api_user', ''),
    '#description' => t('<strong>Note:</strong> this user must have permissions to view projects and create tickets on Unfuddle.'),
    '#required' => TRUE,
  );
  $form['unfuddle_api_pass'] = array(
    '#type' => 'password',
    '#title' => t("Unfuddle user's password"),
    '#default_value' => variable_get('unfuddle_api_pass', ''),
    '#required' => TRUE,
  );
  
  return system_settings_form($form);
}

/**
 * Wrapper function for loading the Unfuddle class and returning an instantiated Unfuddle object. The main API function for use.
 *
 * @param string $url Optional Unfuddle URL.
 * @param string $user Optional Unfuddle user.
 * @param string $pass Optional Unfuddle user's password.
 */
function unfuddle_api_create($url = NULL, $user = NULL, $pass = NULL) {
  module_load_include('classes.inc', 'unfuddle_api');
  
  $unfuddle = new Unfuddle($url, $user, $pass);
  
  return $unfuddle;
}