<?php

/**
 * @file Contains Unfuddle API classes for Unfuddle API Drupal module.
 */

/**
 * Primary Unfuddle API implementation class.
 */
class Unfuddle {
  
  protected $url;
  protected $user;
  protected $pass;
  
  protected $format = 'json';
  
  public function __construct($url = NULL, $user = NULL, $pass = NULL) {
    $this->url = is_null($url) ? variable_get('unfuddle_api_url', '') : $url;
    $this->user = is_null($user) ? variable_get('unfuddle_api_user', '') : $user;
    $this->pass = is_null($pass) ? variable_get('unfuddle_api_pass', '') : $pass;
  }
  
  public function setURL($url) {
    $this->url = $url;
  }
  
  public function setAuth($user, $pass) {
    $this->user = $user;
    $this->pass = $pass;
  }
  
  // Projects.
  public function getProjects() {
    try {
      $projects = $this->request($this->url . '/api/v1/projects');
      return $projects;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle projects. Error message @message', array('@message' => $message), 'error');
      return FALSE;
    }
  }
  
  public function getProject($projectID) {
    try {
      $project = $this->request($this->url . '/api/v1/projects/' . $projectID);
      return $project;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle project !id. Error message @message', array('!id' => $projectID, '@message' => $message), 'error');
      return FALSE;
    }
  }
  
  // People.
  public function getPeople($projectID) {
    try {
      $people = $this->request($this->url . '/api/v1/people');
      return $people;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle people. Error message @message', array('@message' => $message), 'error');
      return FALSE;
    }
  }
  
  public function getProjectPeople($projectID) {
    try {
      $people = $this->request($this->url . '/api/v1/projects/' . $projectID . '/people');
      return $people;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle people for project !id. Error message @message', array('!id' => $projectID, '@message' => $message), 'error');
      return FALSE;
    }
  }
  
  public function getPerson($personID) {
    try {
      $person = $this->request($this->url . '/api/v1/people/' . $personID);
      return $person;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle person !pid. Error message @message', array('!pid' => $personID, '@message' => $message), 'error');
      return FALSE;
    }
  }
  
  // Messages.
  public function getMessages($projectID) {
    try {
      $messages = $this->request($this->url . '/api/v1/projects' . $projectID . '/messages');
      return $messages;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle messages. Error message @message', array('@message' => $message), 'error');
      return FALSE;
    }
  }
  
  public function getMessage($projectID, $messageID) {
    try {
      $message = $this->request($this->url . '/api/v1/projects' . $projectID . '/messages/' . $messageID);
      return $message;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle message !mid for project !id. Error message @message', array('!mid' => $messageID, '!id' => $projectID, '@message' => $message), 'error');
      return FALSE;
    }
  }
  
  public function createMessage($projectID, $title, $body) {
    $message =
    "<message>\n".
    "  <body>" . $body . "</body>\n".
    "  <title>" . $title . "</title>\n".
    "  </message>\n";
    
    // POST message.
    try {
      $response = $this->request($this->url . '/api/v1/projects/' . $projectID . '/messages', $message, 'POST');
      watchdog('unfuddle_api', 'Unfuddle message !title created', array('!title' => $title));
      // Extract the message ID from the Location URL.
      return substr($response->headers['Location'], strrpos($response->headers['Location'], '/') + 1);
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unfuddle message creation failed with error message @message', array('@message' => $message), 'error');
      return FALSE;
    }
  }
  
  public function getCategories($projectID) {
    try {
      $categories = $this->request($this->url . '/api/v1/projects' . $projectID . '/categories');
      return $categories;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle categories. Error message @message', array('@message' => $message), 'error');
      return FALSE;
    }
  }
  
  public function getCategory($projectID, $categoryID) {
    try {
      $category = $this->request($this->url . '/api/v1/projects' . $projectID . '/categories/' . $categoryID);
      return $category;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle category !cid for project !id. Error message @message', array('!cid' => $categoryID, '!id' => $projectID, '@message' => $message), 'error');
      return FALSE;
    }
  }
  
  // Tickets.
  public function getTickets($projectID) {
    try {
      $ticket = $this->request($this->url . '/api/v1/projects/' . $projectID . '/tickets');
      return $ticket;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle tickets for project !id. Error message @message', array('!id' => $projectID, '@message' => $message), 'error');
      return FALSE;
    }
  }
  
  public function getTicket($projectID, $ticketID) {
    try {
      $ticket = $this->request($this->url . '/api/v1/projects/' . $projectID . '/tickets/' . $ticketID);
      return $ticket;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle ticket !tid for project !id. Error message @message', array('!tid' => $ticketID, '!id' => $projectID, '@message' => $message), 'error');
      return FALSE;
    }
  }
  
  public function getTicketByNumber($projectID, $ticketNum) {
    try {
      $ticket = $this->request($this->url . '/api/v1/projects/' . $projectID . '/tickets/by_number/' . $ticketNum);
      return $ticket;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle ticket number !num for project !id. Error message @message', array('!num' => $ticketNum, '!id' => $projectID, '@message' => $message), 'error');
      return FALSE;
    }
  }
  
  public function createTicket($projectID, $summary, $description, $fields = array()) {
    $message =
    "<ticket>\n".
    "  <summary>" . $summary . "</summary>\n".
    "  <description><![CDATA[" . $description . "]]></description>\n".
    "  <priority>5</priority>\n".
    "  <project-id>" . $projectID . "</project-id>\n".
    "  <status>new</status>\n".
    "  </ticket>\n";
    // @TODO handle other fields, maybe create a SimpleXML object?
    
    // POST message.
    try {
      $response = $this->request($this->url . '/api/v1/projects/' . $projectID . '/tickets', $message, 'POST');
      watchdog('unfuddle_api', 'Unfuddle ticket !summary created', array('!summary' => $summary));
      // Extract the ticket ID from the Location URL.
      return substr($response->headers['Location'], strrpos($response->headers['Location'], '/') + 1);
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unfuddle ticket creation failed with error message @message', array('@message' => $message), 'error');
      return FALSE;
    }
  }
  
  
  
  /**
   * Build and perform the request.
   */
  protected function request($url, $data = '', $method = 'GET') {
    $headers = array();
    if ($method == 'GET') {
      if ($data != '') {
        $url .= '?' . http_build_query($data, '', '&');
        $data = '';
      }
      $headers['Content-type'] = 'application/x-www-form-urlencoded';
    }
    else {
      // Leave $data as is, Unfuddle takes XML.
      $headers['Content-type'] = 'application/xml';
    }

    $headers['Authorization'] = 'Basic ' . base64_encode($this->user . ':' . $this->pass);
    switch ($this->format) {
      case 'json':
        $headers['Accept'] = 'application/json';
        break;
      case 'xml':
        $headers['Accept'] = 'application/xml';
        break;
    }
    
    $response = drupal_http_request($url, $headers, $method, $data);
    switch ($response->code) {
      case '200':
        // OK.
        return $this->parseResponse($response->data);
      case '201':
        // Created.
        return $response; // Return the response object. drupal_http_request() does not handle all response codes. See Unfuddle::createTicket() for use.
      case '400':
        // Bad Request.
        throw new Exception($response->error);
    }
  }
  
  protected function parseResponse($response) {
    switch ($this->format) {
      case 'json':
        return json_decode($response, TRUE);
      case 'xml':
        return @simplexml_load_string($response);
    }
  }
}